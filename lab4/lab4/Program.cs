﻿using System;
using System.Diagnostics;
using System.IO;

class Program
{
    static void Main()
    {
        string registryKey = @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run";
        string exportFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "lab4.reg");

        ExportRegistryKey(registryKey, exportFilePath);
        Console.ReadKey();
    }
    static void ExportRegistryKey(string registryKey, string exportFilePath)
    {
        try
        {
            ProcessStartInfo processStartInfo = new ProcessStartInfo
            {
                FileName = "reg.exe",
                Arguments = $"export \"{registryKey}\" \"{exportFilePath}\" /y",
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true
            };

            using (Process process = Process.Start(processStartInfo))
            {
                process.WaitForExit();
                if (process.ExitCode == 0)
                {
                    Console.WriteLine($"Розділ реєстру '{registryKey}' успішно експортовано у файл '{exportFilePath}'.");
                }
                else
                {
                    Console.WriteLine($"Помилка при експорті розділу реєстру. Код помилки: {process.ExitCode}");
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Помилка: {ex.Message}");
        }
    }
}

